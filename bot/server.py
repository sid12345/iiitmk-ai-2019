import re
import os
import bottle
import argparse
import requests
from json import dumps
from datetime import datetime

parser = argparse.ArgumentParser()
parser.add_argument("token", action="store", help="Gitlab Token")
parser.add_argument("secret", action="store", help="Callback Secret Token")
parser.add_argument(
    "instructor", action="store", help="User ID of the course instructor"
)
parser.add_argument(
    "-port", action="store", help="Port to run on", default=os.environ.get("PORT", 8080)
)

args = parser.parse_args()
print("Instructor ID:", args.instructor)
args.port = int(args.port)

app = bottle.Bottle()
gitlab_root = "https://gitlab.com/api/v4"
gitlab_kw = {"headers": {"Private-Token": args.token}}
valid_title = re.compile(r"^#(?:\s+)?(\d+)$")


def bot_speak(msg):
    return f"""{msg}
    
This message was written by the Bot: 🤖"""


@app.post("/gitlabhook")
def push_reaction():
    if bottle.request.headers.get("X-Gitlab-Token") == args.secret:
        json = bottle.request.json
        if json["object_kind"] != "merge_request":
            return bottle.abort(400, "not merge request")
        mr = json["object_attributes"]
        print(mr["iid"], mr["action"], end=" ")
        if mr["action"] != "open":
            return bottle.abort(400, "Not an opening action")
        project_root = f'{gitlab_root}/projects/{json["project"]["id"]}'
        mr_root = f'{project_root}/merge_requests/{mr["iid"]}'
        mr_updates = {"assignee_id": args.instructor}
        if not valid_title.match(mr["title"]):  # invalid title
            print("invalid title", end=" ")
            requests.post(
                f"{mr_root}/notes",
                json={
                    "body": bot_speak(
                        """The title is in the incorrect format.
                    Please refer to <https://gitlab.com/gitcourses/iiitmk-ai-2019/issues/3> to see proper title format.
                    """
                    )
                },
                **gitlab_kw,
            )
            # mr_updates["state_event"] = "close"
        else:  # valid title
            print("title ok", end=" ")
            issue_iid = list(valid_title.search(mr["title"]).groups())[0]
            r = requests.get(f"{project_root}/issues/{issue_iid}", **gitlab_kw)
            due_date = arrow.get(r.json()["due_date"])
            labels = set(mr_updates["labels"].split(","))
            if arrow.now() > due_date:  # late submission
                print("Late submission", end=" ")
                requests.post(
                    f"{mr_root}/notes",
                    json={
                        "body": bot_speak(
                            f"The due date for this assignment was {due_date}."
                        )
                    },
                    **gitlab_kw,
                )
                labels.add("LateWork")
                if "OnTimeWork" in labels:
                    labels.remove("OnTimeWork")
                # mr_updates["state_event"] = "close"
            else:  # on time submission
                print("On Time", end=" ")
                labels.add("OnTimeWork")
                if "LateWork" in labels:
                    labels.remove("LateWork")
            mr_updates["labels"] = ",".join(labels)
        print(mr_updates)
        requests.put(f"{mr_root}", data=mr_updates, **gitlab_kw)


app.run(port=args.port)
